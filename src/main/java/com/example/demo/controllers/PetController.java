package com.example.demo.controllers;

import com.example.demo.models.Pet;
import com.example.demo.repository.PetRepository;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class PetController {

    private final PetRepository petRepository;

    public PetController(PetRepository petRepository) {
        this.petRepository = petRepository;
    }

    @GetMapping("/pets")
    public List<Pet> getPets(){
        return (List<Pet>) petRepository.findAll();
    }

    @GetMapping("/pets/{id}")
    public Pet getPet(@PathVariable Integer id){
        return petRepository.findById(id).get();
    }

    @PostMapping(path = "/pets")
    public void addPet(@RequestBody Pet pet){
        petRepository.save(pet);
    }

    @DeleteMapping(path = "/pets/{id}")
    public void deletePet(@PathVariable Integer id){
        petRepository.deleteById(id);
    }

    @PutMapping(path = "/pets")
    public boolean updatePet(@RequestBody Pet pet){
        System.out.println("updatePet " + pet);

        if(pet.getId() == null)
            return false;
        if(pet.getName() == null)
            return false;
        if(pet.getAge() == null)
            return false;
        if (pet.getColor() == null)
            return false;
        if(pet.getWeight() == null)
            return false;
        if (pet.getType() == null)
            return false;

        Pet oldPet = petRepository.findById(pet.getId()).get();
        oldPet.setNewValue(pet);
        petRepository.save(oldPet);

        return true;

    }


}
