package com.example.demo.models;

import javax.persistence.*;

@Entity
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private Integer age;
    private Integer weight;
    private String color;
    private String type;

    public Pet(){
    }

    public Pet(String name, Integer age, Integer weight, String color, String type){
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.color = color;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setNewValue(Pet newValue){
        this.name = newValue.getName();
        this.age = newValue.getAge();
        this.weight = newValue.getWeight();
        this.color = newValue.getColor();
        this.type = newValue.getType();
    }

    @Override
    public String toString() {
        return "id=" + this.id +
                ", name=" + this.name +
                ", age=" + this.age +
                ", weight=" + this.weight +
                ", color=" + this.color +
                ", type=" + this.type;
    }
}
