package com.example.demo;

import com.example.demo.models.Pet;
import com.example.demo.repository.PetRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {

		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	CommandLineRunner init(PetRepository petRepository) {
		return args -> {
                Pet pet = new Pet("Richard", 12, 10, "black", "Cat");
                Pet pet1 = new Pet("Sharik", 5, 6, "gray", "Dog");
				petRepository.save(pet);
				petRepository.findAll().forEach(System.out::println);
		};
	}
}
