<img src="https://upload.wikimedia.org/wikipedia/uk/thumb/2/2e/Java_Logo.svg/643px-Java_Logo.svg.png" width="64">
<img src="https://i.ya-webdesign.com/images/spring-logo-png-1.png" width="64">


* <h4>Создание MySQL:</h4>  
```
mysql> create database petdb;
mysql> create user 'springuser'@'%' identified by 'ThePassword';
mysql> grant all on db_example.* to 'springuser'@'%';
```




* <h4>Склонируйте проект в выбранную директорию командой:</h4>  
> `git clone https://gitlab.com/service-pets/service-back`
<h4>Запустите проект</h4> 



